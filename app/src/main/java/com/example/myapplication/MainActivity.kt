package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {

    private lateinit var resultTW: TextView
    private lateinit var resultTW2: TextView // ეს ოპერაციის საჩვენებლად

    private var operand: Double = 0.0
    private var operation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTW = findViewById((R.id.resultTW))
        resultTW2 = findViewById((R.id.resultTW2))

    }

    fun numberClick(clickedView: View) {

        if(clickedView is TextView) {

            var result = resultTW.text.toString()
            val result1 = resultTW2.text.toString()
            val number = clickedView.text.toString()

            if (result == "0") {
                result = ""
            }

            resultTW.text = result + number

            resultTW2.text = result1 + number

        }

    }

    fun operationClick(clickedView: View) {

        if (clickedView is TextView) {

            val result = resultTW.text.toString()

            if (result.isNotEmpty()) {
                operand = result.toDouble()
            }

            operation = clickedView.text.toString()

            resultTW.text = ""

            resultTW2.text = resultTW2.text.toString() + operation

        }

    }

    fun equalsClick(clickedView: View) {

        val secOperandText = resultTW.text.toString()
        var secOperand = 0.0

        if (secOperandText.isNotEmpty()) {
            secOperand = secOperandText.toDouble()
        }

        when(operation) {

            "+" -> resultTW.text = (operand + secOperand).toString()
            "-" -> resultTW.text = (operand - secOperand).toString()
            "*" -> resultTW.text = (operand * secOperand).toString()
            "%" -> resultTW.text = ((operand * secOperand) / 100).toString() // პროცენტიც ჩავამატე ბარემ
            "/" -> {
                if (secOperandText == "0") { // ნოლზე გაყოფის ერორი
                    Toast.makeText(applicationContext, "can't divide by zero.", Toast.LENGTH_SHORT).show()
                } else {
                    resultTW.text = (operand / secOperand).toString()
                }
            }

        }

        resultTW2.text = ""

    }

    fun floatClick(clickedView: View) { // წერტილის ფუნქცია

        val result = resultTW.text.toString()

        if("." !in result && result.isNotEmpty()) {
            resultTW.text = "$result."
        }

    }

    fun clearClick(clickedView: View) { // Clear ფუნქცია
        resultTW.text = ""
        resultTW2.text = ""
    }

    fun clearOneNumberClick(clickedView: View) { // Del ფუნქცია
        resultTW.text = resultTW.text.toString().dropLast(1)
    }

    fun rootClick(clickedView: View) { // ფესვის ამოყვანა

        var result = resultTW.text.toString()

        if (result.isNotEmpty()) {

            var sqrt = sqrt(result.toDouble())
            resultTW.text = sqrt.toString()

        } else {
            Toast.makeText(applicationContext, "Enter number.", Toast.LENGTH_SHORT).show()
        }

    }

}